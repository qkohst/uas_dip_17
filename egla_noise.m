clc;
clear;
close all;


pat1 = imread('3.egla_noise.png');
%// Change
spec_orig = fft2(double(pat1)); 
spec_img = fftshift(spec_orig);

for j = 115:125
    for n = 96:106
        spec_img(n,j) = 0; 
    end
    for n = 216:226
        spec_img(n,j) = 0; 
    end
    for n = 274:284
        spec_img(n,j) = 0; 
    end
    for n = 298:308
        spec_img(n,j) = 0; 
    end
    for n = 12:22
        spec_img(n,j) = 0; 
    end
    for n = 37:47
        spec_img(n,j) = 0; 
    end
end

%// Change
ptnfx = real(ifft2(ifftshift(spec_img)));
figure(1);
imshow(pat1);
title('Noise Image');
figure(2);
imshow(spec_orig);
title('Fast Fourier Transform II');
figure(3);
imshow(spec_img);
title('Pergeseran Dari Hasil Fast Fourier Transform');
figure(4);
imshow(ptnfx,[]);
title('Inverse Fast Fourier Tansform');